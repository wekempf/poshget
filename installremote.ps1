function InstallRemote([uri]$RemoteUrl, $Destination, [switch]$Update) {
    Write-Verbose "Installing from remote URL '$RemoteUrl'..."
    $fileName = [System.IO.Path]::GetFileName($RemoteUrl.LocalPath)
    $filePath = Join-Path $env:TEMP $fileName
    Write-Verbose "Downloading to '$filePath'"
    $client = New-Object System.Net.WebClient
    $client.DownloadFile($RemoteUrl, $filePath)
    try {
        InstallLocal -LocalPath:$filePath -Destination:$Destination -Update:$Update
    } finally {
        Remove-Item $filePath
    }
}


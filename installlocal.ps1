function InstallLocal {
    param(
        $ModuleInfo,
        $Path,
        [string]$Destination,
        [switch]$Update
    )
    if ($ModuleInfo.IsInstalled) {
        if (-not $Update) {
            Write-Verbose 'Module already installed'
            return
        }
        if ((Split-Path $ModuleInfo.InstalledTo) -ne $Destination) {
            throw "Module '$($ModuleInfo.Name)' is already installed in a different location"
        }
    }
    $item = Get-Item $Path
    if (-not (Test-Path $Destination)) {
        Write-Verbose "Creating destination directory '$Destination'"
        New-Item -ItemType Directory $Destination | Out-Null
    }
    $modulePath = Join-Path $Destination $ModuleInfo.Name
    if (Test-Path $modulePath) {
        Write-Verbose "Removing existing module directory '$modulePath'"
        Remove-Item $modulePath -Recurse -Force | Out-Null
    }
    switch ($ModuleInfo.Type) {
        'application/x-moduledir' {
            Write-Verbose "Copying module directory '$item'"
            Copy-Item $item $Destination
        }
        'application/zip' {
            $shellApp = New-Object -Com Shell.Application
            $zipFile = $shellApp.namespace($item.FullName)
            $moduleItem = $zipFile.Items() | Where-Object { $_.Name.StartsWith(($ModuleInfo.ModuleName)) -and $_.IsFolder }
            Write-Debug $moduleItem.Title
            if (-not $moduleItem) {
                throw 'Source does not appear to be a module archive'
            }
            $moduleScript = $moduleItem.GetFolder.Items() | Where-Object { $_.Name -eq "$($ModuleInfo.ModuleName).psm1" -and (-not $_.IsFolder) }
            Write-Debug $moduleScript.Title
            if (-not $moduleScript) {
                throw 'Source does not appear to be a module archive'
            }
            Write-Verbose "Unpacking to '$Destination'"
            $dest = $shellApp.namespace($Destination)
            Write-Debug $dest.Title
            $dest.Copyhere($zipFile.items())
        }
        'text/plain' {
            Write-Verbose "Creating module directory '$modulePath'"
            New-Item -ItemType Directory $modulePath
            Write-Verbose "Copying module script '$item'"
            Copy-Item $item (Join-Path $modulePath $item.BaseName+'.psm1')
        }
        default {
            throw "Unknown module type"
        }
    }
    if (-not $Update) {
        SaveToInstalled $ModuleInfo $Destination
    }
}

function TestModulePath {
    param(
        [string]$Path
    )

    if (Test-Path $Path) {
        $item = Get-Item $Path -ErrorAction Stop
        if ($item.PSProvider.Name -eq 'FileSystem') {
            if ($item.PSIsContainer) {
                if (Test-Path (Join-Path $item "$($item.BaseName).psm1")) {
                    return $true
                }
            } elseif ($item.Extension -in @('.ps1','.psm1','.zip')) {
                return $true
            }
        }
    }
}

function GetLocalModuleInfo {
    param(
        [string]$Path
    )
    $item = Get-Item $Path -ErrorAction Stop
    $moduleName = $item.BaseName
    $moduleInfo = Get-ModuleInfo $moduleName -ErrorAction SilentlyContinue
    if ($moduleInfo) {
        if ($moduleInfo.Url -ne (Resolve-Path $Path)) {
            throw "A module named '$moduleName' from a different source is already installed"
        }
    } else {
        $moduleInfo = NewModuleInfo2
        $moduleInfo.Name = $moduleName
        $moduleInfo.Title = $moduleName
        $moduleInfo.Summary = 'local module'
        if ($item.PSIsContainer) {
            $moduleInfo.Type = 'application/x-moduledir'
        } elseif ($item.Extension -eq '.zip') {
            $moduleInfo.Type = 'application/zip'
        } else {
            $moduleInfo.Type = 'text/plain'
        }
        $moduleInfo.Url = New-Object System.Uri -ArgumentList $item.FullName,'Absolute'
    }
    $moduleInfo
}


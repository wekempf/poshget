Add-Type -AssemblyName System.ServiceModel
Add-Type -AssemblyName System.Xml.Linq

$dataDir = Join-Path ([Environment]::GetFolderPath("LocalApplicationData")) PoshGet
$installedFile = Join-Path $dataDir installed.xml
$knownModulesFile = Join-Path $PSScriptRoot known.xml
$psGetNs = [System.Xml.Linq.XNamespace]'urn:psget:v1.0'
$poshGetNs = [System.Xml.Linq.XNamespace]'urn:poshget:v1.0'

<#
.Synopsis
    Installs a module.
.DESCRIPTION
    Installs a module with support for modules that are found remotely.
.EXAMPLE
    Install-Module posh-git

    Description
    -----------
    Installs the well known module posh-git.
.EXAMPLE
    Install-Module -GitRepository https://wekempf@bitbucket.org/wekempf/psprompt.git

    Description
    -----------
    Installs the psprompt module found in a Git repository.
.EXAMPLE
    Install-Module -Name posh-git -Update -Import

    Description
    -----------
    Installs the well known module posh-git, updates it if it was already installed and then imports the module.
.LINK
    https://bitbucket.org/wekempf/poshget
    Uninstall-Module
    Get-ModuleInfo
#>
function Install-Module
{
    [CmdletBinding(DefaultParameterSetName='named')]
    [OutputType([void])]
    Param
    (
        # Name of the well known module to install.
        [Parameter(Mandatory=$true,
            ParameterSetName='named',
            ValueFromPipelineByPropertyName=$true,
            Position=0)]
        [Alias('Name')]
        [ValidateNotNullOrEmpty()]
        [string[]]
        $ModuleName,

        # Remote Url to module to install.
        [Parameter(Mandatory=$true,
            ParameterSetName='web',
            ValueFromPipelineByPropertyName=$true,
            Position=0)]
        [Uri[]]
        $RemoteUrl,

        # Local path to module to install.
        [Parameter(Mandatory=$true,
            ParameterSetName='local',
            ValueFromPipelineByPropertyName=$true,
            Position=0)]
        [ValidateScript({ TestModulePath $_ })]
        [string[]]
        $Path,

        # Git repository containing module to install.
        [Parameter(Mandatory=$true,
            ParameterSetName='git',
            ValueFromPipelineByPropertyName=$true,
            Position=0)]
        [Uri[]]$GitRepository,

        # Destination path to install module to.
        [Parameter(Mandatory=$false,
            Position=1)]
        [ValidateNotNullOrEmpty()]
        [string]$Destination = $null,

        # Update the module if it is already installed.
        [switch]
        $Update,

        # Import the module when done.
        [switch]
        $Import
    )

    Begin {
        trap {
            break
        }
        if ($PSVersionTable.PSVersion.Major -lt 2) {
            throw "PoshGet requires PowerShell 2.0 or better; you have version $($Host.Version)"
        }
        if (-not $Destination) {
            $Destination = GetDefaultDestination
        }
        if (-not ($env:PSModulePath.Split(';') -contains $Destination)) {
            Write-Warning 'Destination is not in PSModulePath.'
        }
    }

    Process {
        switch ($PSCmdlet.ParameterSetName) {
            named {
                Write-Verbose 'Named'
                $module = $ModuleName | ForEach-Object { InstallNamed -ModuleName:$_ -Destination:$Destination -Update:$Update }
                Write-Verbose 'Installed'
            }
            web {
                $module = $RemoteUrl | ForEach-Object { InstallWeb -RemoteUrl:$_ -Destination:$Destination -Update:$Update }
            }
            local {
                $Path | ForEach-Object {
                    $module = GetLocalModuleInfo $_
                    InstallLocal $module $module.Url.AbsolutePath $Destination -Update:$Update
                }
            }
            git {
                $module = $GitRepository | ForEach-Object { InstallGit -GitRepository:$GitRepository -Destination:$Destination -Update:$Update }
            }
        }

        Write-Verbose 'Import: $Import'
        if ($Import) {
            Write-Verbose 'Importing...'
            $module | ForEach-Object {
                Write-Verbose "Importing '$($_.Name)'..."
                Import-Module $_.Path -Scope:Global -Force 2>&1 | Out-Null
            }
        }
    }

    End {
    }
}

<#
.Synopsis
    Get information about known modules.
.DESCRIPTION
    Command queries information about modules from some central directories and from the installed modules.
.EXAMPLE
    Get-ModuleInfo

    Description
    -----------
    Gets the information for installed modules.
.EXAMPLE
    Get-ModuleInfo -ListAvailable

    Description
    -----------
    Gets the information for all available (including installed) modules.
.EXAMPLE
    Get-ModuleInfo posh-git

    Description
    -----------
    Gets the information for the posh-git module. Returns nothing if posh-git is not installed.
.EXAMPLE
    Get-ModuleInfo posh-git -ListAvailable

    Description
    -----------
    Gets the information for the posh-git module.
.LINK
    https://bitbucket.org/wekempf/poshget
    Install-Module
    Uninstall-Module
#>
function Get-ModuleInfo
{
    [CmdletBinding()]
    [OutputType([PSObject])]
    Param
    (
        # The name of the module to get info for
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        [Alias('Name')]
        [string[]]$ModuleName,

        # Gets only installed module info
        [switch]$ListAvailable
    )

    Begin {
        $ErrorActionPreference = 'Stop'
    }

    Process {
        (GetModuleInfo).GetEnumerator() | ForEach-Object { $_.Value } | Where-Object {
            ((-not $ModuleName) -or $_.Name -like $ModuleName) -and ($ListAvailable -or $_.InstalledTo)
        }
    }

    End {
    }
}

<#
.Synopsis
    Uninstalls a module.
.EXAMPLE
    Uninstall-Module posh-git

    Description
    -----------
    Uninstalls the posh-git module.
.LINK
    https://bitbucket.org/wekempf/poshget
    Install-Module
    Get-ModuleInfo
#>
function Uninstall-Module
{
    [CmdletBinding()]
    [OutputType([void])]
    Param
    (
        # The name of the module to uninstall.
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        [Alias('Name')]
        $ModuleName
    )

    Begin {
        trap {
            break
        }
    }

    Process {
        $feed = GetInstalledFeed
        $entry = $feed.Items | Where-Object { $_.Id -eq $ModuleName }
        if (-not $entry) {
            throw "Module '$ModuleName' is not installed."
        }
        $info = NewModuleInfo -SyndicationItem:$entry
        Write-Verbose "Removing '$(Join-Path $info.InstalledTo $info.Name)'..."
        rm -rec -force (Join-Path $info.InstalledTo $info.Name) | Out-Null
        Write-Verbose "Removing entry from '$installedFile'..."
        $feed.Items.Remove($entry) | Out-Null
        $writer = [System.Xml.XmlWriter]::Create($installedFile)
        try {
            $feed.SaveAsAtom10($writer)
        } finally {
            $writer.Dispose()
        }
    }

    End {
    }
}

. $PSScriptRoot\installnamed.ps1
. $PSScriptRoot\installlocal.ps1
. $PSScriptRoot\installremote.ps1
. $PSScriptRoot\installgit.ps1
. $PSScriptRoot\utility.ps1
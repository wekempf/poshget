Add-Type -AssemblyName System.ServiceModel

function ?? {
    $args | Where-Object { $_ -ne $null } | Select-Object -First 1
}

function NewModuleInfo {
    param(
        [System.Collections.Hashtable]$Properties,
        [System.ServiceModel.Syndication.SyndicationItem]$SyndicationItem
    )
    $info = New-Object PSObject -Property @{
        Name = [string]$Properties.Name
        Title = [string]$Properties.Title
        Summary = [string]$Properties.Summary
        Updated = ?? [DateTimeOffset]$Properties.Updated ([DateTimeOffset]::Now)
        Authors = $Properties.Authors
        Type = [string]$Properties.Type
        Url = [Uri]$Properties.Url
        ProjectUrl = [Uri]$Properties.ProjectUrl
        InstalledTo = [string]$Properties.InstalledTo
    }
    if ($SyndicationItem) {
        $info.Name = $SyndicationItem.Id
        $info.Title = $SyndicationItem.Title.Text
        $info.Summary = $SyndicationItem.Summary.Text
        $info.Updated = $SyndicationItem.LastUpdatedTime
        $info.Authors = NewAuthors $SyndicationItem.Authors
        $info.Type = $SyndicationItem.Content.Type
        $info.Url = $SyndicationItem.Content.Url
        $info.ProjectUrl = GetProjectUrl $SyndicationItem.ElementExtensions
        $info.InstalledTo = GetInstalledTo $SyndicationItem.ElementExtensions $InstalledTo
    }
    Add-Member -InputObject $info -MemberType ScriptProperty -Name Path { if ($this.InstalledTo) { Join-Path $this.InstalledTo $this.Name } }

    # Define which properties to display by default
    $defaultProperties = [string[]]@('Name','Summary')
    $defaultDisplayPropertySet = New-Object System.Management.Automation.PSPropertySet('DefaultDisplayPropertySet',$defaultProperties)
    $PSStandardMembers = [System.Management.Automation.PSMemberInfo[]]@($defaultDisplayPropertySet)
    Add-Member -InputObject $info -MemberType MemberSet -Name PSStandardMembers -Value $PSStandardMembers

    # Give the object a type name
    $info.PSObject.TypeNames.Insert(0,'PoshGetModuleInfo')

    $info
}

function UseLocation($Path, [ScriptBlock]$ScriptBlock) {
    Push-Location $Path
    try {
        &$ScriptBlock
    } finally {
        Pop-Location
    }
}

function GetInstalledFeed {
    if (Test-Path $installedFile) {
        $feed = GetFeed $installedFile
    } else {
        if (-not (Test-Path $dataDir)) {
            md $dataDir | Out-Null
        }
        $feed = New-Object System.ServiceModel.Syndication.SyndicationFeed
        $poshGetXmlns = New-Object System.Xml.XmlQualifiedName -ArgumentList 'poshget',([System.Xml.Linq.XNamespace]::Xmlns)
        $feed.AttributeExtensions.Add($poshgetXmlns, $poshGetNs)
        $feed.Title = New-Object System.ServiceModel.Syndication.TextSyndicationContent -ArgumentList 'Installed PowerShell Modules'
    }
    $feed
}

function GetFeed($Url) {
    $reader = [System.Xml.XmlReader]::Create($Url)
    try {
        [System.ServiceModel.Syndication.SyndicationFeed]::Load($reader)
    } catch [System.Xml.XmlException] {
        Write-Error $_.Exception.InnerException
    } finally {
        $reader.Dispose()
    }
}

function GetModuleInfo() {
    $info = @{ }
    (GetFeed https://raw.github.com/psget/psget/master/Directory.xml).Items | ForEach-Object { $info[$_.Id] = NewModuleInfo -SyndicationItem:$_ }
    if (Test-Path $knownModulesFile) {
        (GetFeed $knownModulesFile).Items | ForEach-Object { $info[$_.Id] = NewModuleInfo -SyndicationItem:$_ }
    }
    if (Test-Path $installedFile) {
        (GetFeed $installedFile).Items | ForEach-Object { $info[$_.Id] = NewModuleInfo -SyndicationItem:$_ }
    }
    $info
}


function NewSyndicationItem {
    param(
        $ModuleInfo
    )
    $item = New-Object System.ServiceModel.Syndication.SyndicationItem
    $item.Id = $ModuleInfo.Name
    $item.Title = $ModuleInfo.Title
    $item.Summary = $ModuleInfo.Summary
    $item.LastUpdatedTime = $ModuleInfo.Updated
    $ModuleInfo.Authors | ForEach-Object {
        $item.Authors.Add((New-Object System.ServiceModel.Syndication.SyndicationPerson -ArgumentList $_.Email,$_.Name,$_.Uri))
    }
    $item.Content = New-Object System.ServiceModel.Syndication.UrlSyndicationContent -ArgumentList $ModuleInfo.Url,$ModuleInfo.Type
    if ($ModuleInfo.ProjectUrl) {
        $item.ElementExtensions.Add('properties', $psGetNs, $ModuleInfo.ProjectUrl)
    }
    if ($ModuleInfo.InstalledTo) {
        $installedTo = New-Object System.Xml.Linq.XElement -ArgumentList ($poshGetNs + 'InstalledTo'),$ModuleInfo.InstalledTo
        $item.ElementExtensions.Add('properties', $poshGetNs, $installedTo)
    }
    $item
}

function NewAuthors {
    param(
        [System.ServiceModel.Syndication.SyndicationPerson[]]$SyndicationPerson
    )
    $SyndicationPerson | ForEach-Object { NewAuthor -SyndicationPerson:$_ }
}

function NewAuthor {
    param(
        [string]$Name,
        [Uri]$Uri,
        [string]$Email,
        [System.ServiceModel.Syndication.SyndicationPerson]$SyndicationPerson
    )
    $author = New-Object PSObject -Property @{
        Name = $Name
        Uri = $Uri
        Email = $Email
    }
    if ($SyndicationPerson) {
        $author.Name = $SyndicationPerson.Name
        $author.Uri = $SyndicationPerson.Uri
        $author.Email = $SyndicationPerson.Email
    }

    # Define which properties to display by default
    $defaultProperties = [string[]]@('Name')
    $defaultDisplayPropertySet = New-Object System.Management.Automation.PSPropertySet('DefaultDisplayPropertySet',$defaultProperties)
    $PSStandardMembers = [System.Management.Automation.PSMemberInfo[]]@($defaultDisplayPropertySet)
    Add-Member -InputObject $author -MemberType MemberSet -Name PSStandardMembers -Value $PSStandardMembers -Force

    # Give the object a type name
    $author.PSObject.TypeNames.Insert(0,'PoshGetAuthor')

    $author
}

function GetProjectUrl($FeedElementExtensions) {
    $FeedElementExtensions | ForEach-Object {
        $reader = $_.GetReader()
        try {
            if (($reader.NamespaceURI -eq $psGetNs -or $reader.NamespaceURI -eq $poshGetNs) -and $reader.LocalName -eq 'properties') {
                while ($reader.Read()) {
                    if ($reader.IsStartElement()) {
                        if (($reader.NamespaceURI -eq $psGetNs -or $reader.NamespaceURI -eq $poshGetNs) -and $reader.LocalName -eq 'ProjectUrl') {
                            return $reader.ReadElementString()
                        }
                    }
                }
            }
        } finally {
            $reader.Dispose()
        }
    }
}

function GetInstalledTo($FeedElementExtensions, $InstalledTo = $null) {
    if ($InstalledTo) {
        return $InstalledTo
    }
    $FeedElementExtensions | ForEach-Object {
        $reader = $_.GetReader()
        try {
            if ($reader.NamespaceURI -eq $poshGetNs -and $reader.LocalName -eq 'properties') {
                while ($reader.Read()) {
                    if ($reader.IsStartElement()) {
                        if ($reader.NamespaceURI -eq $poshGetNs -and $reader.LocalName -eq 'InstalledTo') {
                            return $reader.ReadElementString()
                        }
                    }
                }
            }
        } finally {
            $reader.Dispose()
        }
    }
}

function GetDefaultDestination() {
    $profileDir = Split-Path $profile.CurrentUserAllHosts
    Join-Path $profileDir "Modules"
}

function AddTypeInfo {
    param(
        $InputObject,
        [string]$TypeName,
        [string[]]$DefaultDisplayProperties
    )
    $defaultDisplayPropertySet = New-Object System.Management.Automation.PSPropertySet('DefaultDisplayPropertySet',$DefaultDisplayProperties)
    $PSStandardMembers = [System.Management.Automation.PSMemberInfo[]]@($defaultDisplayPropertySet)
    Add-Member -InputObject $InputObject -MemberType MemberSet -Name PSStandardMembers -Value $PSStandardMembers
    $InputObject.PSObject.TypeNames.Insert(0,$TypeName)
}

function NewAuthor2 {
    $result = New-Object PSObject
    $result | Add-Member -MemberType NoteProperty -Name 'Email' -Value $Null
    $result | Add-Member -MemberType NoteProperty -Name 'Name' -Value $Null
    $result | Add-Member -MemberType NoteProperty -Name 'Uri' -Value $Null
    AddTypeInfo $result 'PoshGetAuthor2' 'Name'
    $result
}

function NewModuleInfo2 {
    $result = New-Object PSObject
    $result | Add-Member -MemberType NoteProperty -Name 'Name' -Value $null
    $result | Add-Member -MemberType NoteProperty -Name 'Title' -Value $null
    $result | Add-Member -MemberType NoteProperty -Name 'Summary' -Value $null
    $result | Add-Member -MemberType NoteProperty -Name 'Updated' -Value ([DateTimeOffset]::Now)
    $result | Add-Member -MemberType NoteProperty -Name 'Authors' -Value @()
    $result | Add-Member -MemberType NoteProperty -Name 'Type' -Value $null
    $result | Add-Member -MemberType NoteProperty -Name 'Url' -Value $null
    $result | Add-Member -MemberType NoteProperty -Name 'ProjectUrl' -Value $null
    $result | Add-Member -MemberType NoteProperty -Name 'InstalledTo' -Value $null
    $result | Add-Member -MemberType ScriptProperty -Name IsInstalled -Value { -not [string]::IsNullOrEmpty($this.InstalledTo) }
    AddTypeInfo $result 'PoshGetModuleInfo2' 'Name','Summary'
    $result
}

function NewModuleInfoFromSyndicationItem {
    param(
        [System.ServiceModel.Syndication.SyndicationItem]$SyndicationItem
    )
    $result = NewModuleInfo2
    $result.Name = $SyndicationItem.Id
    $result.Title = $SyndicationItem.Title.Text
    $result.Summary = $SyndicationItem.Summary.Text
    $result.Updated = $SyndicationItem.LastUpdatedTime
    foreach ($person in $SyndicationItem.Authors) {
        $author = NewAuthor2
        $author.Email = $person.Email
        $author.Name = $person.Name
        $author.Uri = $person.Uri
        $result.Authors.Add($author)
    }
    $result.Type = $SyndicationItem.Content.Type
    $result.Url = $SyndicationItem.Content.Url
    foreach ($extension in $SyndicationItem.ElementExtensions) {
        $reader = $_.GetReader()
        try {
            if (($reader.NamespaceURI -eq $psGetNs -or $reader.NamespaceURI -eq $poshGetNs) -and $reader.LocalName -eq 'properties') {
                while ($reader.Read()) {
                    if ($reader.IsStartElement()) {
                        if (($reader.NamespaceURI -eq $psGetNs -or $reader.NamespaceURI -eq $poshGetNs) -and $reader.LocalName -eq 'ProjectUrl') {
                            $result.ProjectUrl = $reader.ReadElementString()
                        } elseif ($reader.NamespaceURI -eq $poshGetNs -and $reader.LocalName -eq 'InstalledTo') {
                            $result.InstalledTo = $reader.ReadElementString()
                        }
                    }
                }
            }
        } finally {
            $reader.Dispose()
        }
    }
    $result
}

function NewSyndicationItemFromModuleInfo {
    param(
        $ModuleInfo
    )
    $result = New-Object System.ServiceModel.Syndication.SyndicationItem
    $result.Id = $ModuleInfo.Name
    $result.Title = $ModuleInfo.Title
    $result.Summary = $ModuleInfo.Summary
    $result.LastUpdatedTime = $ModuleInfo.Updated
    foreach ($author in $ModuleInfo.Authors) {
        $result.Authors.Add((New-Object System.ServiceModel.Syndication.SyndicationPerson -ArgumentList $author.Email,$author.Name,$author.Uri))
    }
    $result.Content = New-Object System.ServiceModel.Syndication.UrlSyndicationContent -ArgumentList $ModuleInfo.Url,$ModuleInfo.Type
    if ($ModuleInfo.ProjectUrl -or $ModuleInfo.InstalledTo) {
        $extensionElement = New-Object System.Xml.Linq.XElement -ArgumentList ($poshGetNs + 'properties')
        if ($ModuleInfo.ProjectUrl) {
            $projectUrlElement = New-Object System.Xml.Linq.XElement -ArgumentList ($poshGetNs+'ProjectUrl'),$ModuleInfo.ProjectUrl
            $extensionElement.Add($projectUrlElement)
        }
        if ($ModuleInfo.InstalledTo) {
            $installedToElement = New-Object System.Xml.Linq.XElement -ArgumentList ($poshGetNs+'InstalledTo'),$ModuleInfo.InstalledTo
            $extensionElement.Add($installedToElement)
        }
        $result.ElementExtensions.Add($extensionElement.CreateReader())
    }
    $result
}

function SaveToInstalled {
    param(
        $ModuleInfo,
        [string]$Destination
    )
    $ModuleInfo.InstalledTo = Join-Path $Destination $ModuleInfo.Name
    $feed = GetInstalledFeed
    $item = NewSyndicationItemFromModuleInfo $ModuleInfo
    $feed.Items.Add($item)
    $writer = [System.Xml.XmlWriter]::Create($installedFile)
    try {
        Write-Verbose 'Saving installed module info'
        $feed.SaveAsAtom10($writer)
    } finally {
        $writer.Dispose()
    }
}

function EnsureDirectory {
    [CmdletBinding()]
    param(
        [ValidateNotNullOrEmpty()]
        [string]$Path
    )
    if (-not (Test-Path $Path)) {
        Write-Verbose "Creating directory '$Path'"
        New-Item -ItemType Directory -Path $Path
    } else {
        Write-Verbose "Directory '$Path' exists"
        $item = Get-Item -Path $Path -ErrorAction Stop
        if (-not $item.PSIsContainer) {
            throw "'$Path' is not a directory."
        }
        $item
    }
}

function InstallFile {
    [CmdletBinding()]
    param(
        [ValidateScript({ Test-Path -Type Leaf -Path $_ })]
        [string]$Path,
        [string]$Destination
    )
    $Destination = EnsureDirectory $Destination -ErrorAction Stop
    $source = Get-Item $Path
    $moduleDir = EnsureDirectory (Join-Path $Destination $source.BaseName)
    Copy-Item $source (Join-Path $moduleDir "$($source.BaseName).psm1") | Out-Null
    $source.BaseName
}

function InstallFolder {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true, Position=0)]
        [ValidateScript({ TestModuleDirectory $_ })]
        [string]$Path,
        [Parameter(Mandatory=$true, Position=1)]
        [string]$Destination
    )
    $Destination = EnsureDirectory $Destination -ErrorAction Stop
    $name = GetModuleNameFromDirectory $Path
    $moduleDir = EnsureDirectory (Join-Path $Destination $name) -ErrorAction Stop
    Copy-Item (Join-Path $Path *) $moduleDir
    $name
}

function InstallApplicationZip {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true, Position=0)]
        $ModuleInfo,

        [Parameter(Mandatory=$true, Position=1)]
        [string]$Destination
    )
    Write-Verbose "Installing module to '$Destination'"
    $item = Get-Item $ModuleInfo.Url.AbsolutePath -ErrorAction Stop
    $temp = Join-Path $env:TEMP $item.BaseName
    EnsureDirectory $temp -ErrorAction Stop | Out-Null
    try {
        Unzip $item $temp -ErrorAction Stop
        $moduleDir = @(FindModuleDirectory $temp)
        Write-Verbose $moduleDir.Count
        if ($moduleDir.Count -eq 1) {
            InstallFolder $moduleDir[0] $Destination
        }
    } finally {
        Remove-Item $temp -Recurse -Force
    }
}

function FindModuleDirectory {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true, Position=0)]
        [string]$Path
    )
    if (TestModuleDirectory $Path) {
        $Path
    }
    (Get-ChildItem $Path -Recurse | Where-Object { TestModuleDirectory $_.FullName } | Select-Object -First 1).FullName
}

function TestModuleDirectory {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true, Position=0)]
        [string]$Path
    )
    if (GetModuleNameFromDirectory $Path) {
        $true
    } else {
        $false
    }
}

function GetModuleNameFromDirectory {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true, Position=0)]
        [string]$Path
    )
    if (Test-Path -PathType Container $Path) {
        $files = @()
        if (Test-Path -PathType Leaf (Join-Path $Path *.psd1)) {
            $files = @(Get-ChildItem (Join-Path $Path *.psd1))
        } elseif (Test-Path -PathType Leaf (Join-Path $Path *.psm1)) {
            $files = @(Get-ChildItem (Join-Path $Path *.psm1))
        }
        if ($files.Count -eq 1) {
            $files.BaseName
        }
    }
}

function OpenZipFile {
    [CmdletBinding()]
    param(
        [ValidateScript({ Test-Path -Type Leaf -Path $_ })]
        [string]$Path
    )
    $Path = (Resolve-Path $Path).Path
    (New-Object -ComObject Shell.Application).NameSpace($Path)
}

function Unzip {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true, Position=0)]
        [ValidateScript({ Test-Path -Type Leaf -Path $_ })]
        [string]$Path,

        [Parameter(Mandatory=$true, Position=1)]
        [ValidateScript({ Test-Path -Type Container -Path $_ })]
        [string]$Destination
    )
    $zip = OpenZipFile $Path -ErrorAction Stop
    $dest = (New-Object -ComObject Shell.Application).NameSpace((Resolve-Path $Destination).Path)
    $zip.Items() | ForEach-Object { $dest.CopyHere($_) }
}

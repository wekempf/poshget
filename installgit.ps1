function InstallGit($GitRepository, $Destination, [switch]$Update) {
    Write-Verbose "Installing from Git repository '$GitRepository'..."
    if (-not (Get-Command git)) {
        throw 'Git is either not installed or not in the path.'
    }
    $moduleName = [system.io.path]::GetFileNameWithoutExtension((Split-Path $GitRepository -Leaf))
    $module = Get-ModuleInfo $moduleName
    if ($module) {
        Write-Verbose 'Module is already installed'
        if ($Update) {
            UseLocation (Join-Path $module.InstalledTo $module.Name) {
                Write-Verbose 'Pulling...'
                git pull 2>&1 | Out-Null
                if ($LASTEXITCODE) {
                    throw 'Git pull failed.'
                }
            }
        }
        $module
    } else {
        if (-not (Test-Path $Destination)) {
            md $Destination | Out-Null
        }
        UseLocation $Destination {
            Write-Verbose 'Cloning...'
            if (Test-Path $moduleName) {
                Write-Verbose 'Clone failed'
                throw "Cannot clone repository because '$(Join-Path $Destination $moduleName)' already exists."
            }
            git clone $GitRepository 2>&1 | Out-Null
            if ($LASTEXITCODE) {
                Write-Verbose 'Clone failed'
                throw 'Git clone failed.'
            }
            Write-Verbose 'Clone success'
            $module = NewModuleInfo -Name:$moduleName -Title:$moduleName -Summary:'' -Updated:([DateTimeOffset]::Now) -Authors:(NewAuthor -Name:'Machine Generated') -Type:'application/x-git' -Url:$GitRepository -InstalledTo:$Destination
            $item = NewSyndicationItem -ModuleInfo:$module
            $feed = GetInstalledFeed
            $feed.Items.Add($item)
            $writer = [System.Xml.XmlWriter]::Create($installedFile)
            try {
                Write-Verbose 'Saving installed module info...'
                $feed.SaveAsAtom10($writer)
            } finally {
                $writer.Dispose()
            }
            Write-Verbose 'Cloned'
            $module
        }
    }
}

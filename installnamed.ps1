function InstallNamed($ModuleName, $Destination, [switch]$Update) {
    Write-Verbose "Installing module named '$ModuleName'..."
    $module = Get-ModuleInfo -ModuleName:$ModuleName -ListAvailable
    if (-not $module) {
        throw "Cannot find module '$ModuleName'."
    }
    if ($module.InstalledTo) {
        Write-Verbose "Module '$ModuleName' is already installed."
        if (-not $Update) {
            return $module
        }
    }
    switch ($module.Type) {
        "application/x-git" {
            $module = InstallGit -GitRepository:($module.Url) -Destination:$Destination -Update:$Update
        }
        "application/x-moduledir" {
            InstallLocal $module $module.Url.AbsolutePath $Destination -Update:$Update
        }
        "text/plain" {
            if ($module.Url.ToString().StartsWith('file://')) {
                InstallLocal $module $module.Url.AbsolutePath $Destination -Update:$Update
            } else {
                $module = InstallRemote -RemoteUrl:($module.Url) -Destination:$Destination -Update:$Update
            }
        }
        "application/zip" {
            $module = InstallRemote -RemoteUrl:($module.Url) -Destination:$Destination -Update:$Update
        }
    }
    $module
}

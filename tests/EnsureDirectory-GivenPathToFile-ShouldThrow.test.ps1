. $PSScriptRoot\..\utility.ps1
Mock Test-Path { $true }
Mock Get-Item {
    New-Object PSObject -Property @{ PSIsContainer = $false }
}
ExpectException string { EnsureDirectory 'foo' }
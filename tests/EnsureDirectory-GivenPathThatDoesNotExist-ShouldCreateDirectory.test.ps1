. $PSScriptRoot\..\utility.ps1
$expected = 'xyzzy'
Mock Test-Path { $false }
Mock New-Item { $Path }
$actual = EnsureDirectory $expected
Assert { $actual -eq $expected } "Did not create directory. '$result'."
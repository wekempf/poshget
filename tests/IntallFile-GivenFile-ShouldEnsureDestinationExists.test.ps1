. $PSScriptRoot\..\utility.ps1
$global:ensureDirectoryCalled = $false
Mock Test-Path { $true }
Mock EnsureDirectory { $global:ensureDirectoryCalled = $true; throw 'xyzzy' }
ExpectException string { InstallFile foo . }
Assert { $global:ensureDirectoryCalled }
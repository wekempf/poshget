. $PSScriptRoot\..\utility.ps1
$item = EnsureDirectory .
Assert { ($item -is [System.IO.DirectoryInfo]) -and ($item.Path -eq (Get-Item .).Path) } 'Did not get item.'
. $PSScriptRoot\..\poshget.ps1

$global:pushedLocation = $null
Mock Push-Location { $global:pushedLocation = $Path }
Mock Pop-Location { }
UseLocation $env:Temp {
    Assert { $global:pushedLocation -eq $env:Temp } "Failed to push the location. '$global:pushedLocation'"
}
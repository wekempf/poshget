. $PSScriptRoot\..\utility.ps1
$global:installedFile = $null
Mock Copy-Item { $global:installedFile = $Destination }
Mock EnsureDirectory { $Path }
InstallFile $PSScriptRoot\..\utility.ps1 . | Out-Null
Assert { $global:installedFile -eq '.\utility\utility.psm1' } "Did not install module. '$global:installedFile'"
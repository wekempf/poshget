. $PSScriptRoot\..\utility.ps1
$module = NewModuleInfo @{
    Type = 'application/zip'
    Url = "$PSScriptRoot\example.zip"
}
try {
    InstallApplicationZip $module "$PSScriptRoot\modules"
    Assert { Get-Item "$PSScriptRoot\modules\example" -ErrorAction SilentlyContinue }
    Assert { $module.Name -eq 'Example' }
} finally {
    Remove-Item "$PSScriptRoot\modules" -Recurse -Force
}
. $PSScriptRoot\..\poshget.ps1

$global:popped = $false
Mock Push-Location { }
Mock Pop-Location { $global:popped = $true }
UseLocation $env:Temp {
    Assert { -not $global:popped } 'Popped the location before calling script.'
}
Assert { $global:popped } 'Failed to pop the location.'
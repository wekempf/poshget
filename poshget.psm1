#requires -Version 2.0

. $PSScriptRoot\poshget.ps1

Export-ModuleMember -Function Install-Module
Export-ModuleMember -Function Get-ModuleInfo
Export-ModuleMember -Function Uninstall-Module

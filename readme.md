PoshGet
=======

A PowerShell module that provides cmdlets that can be used to install, uninstall and manage modules.

Modules can be installed from local locations, from the web or even from version control repositories (currently only Git is supported).

Usage
-----

See `Get-Help about_PoshGet` for more information.

Installing
----------

0. Verify you have PowerShell 2.0 or better with $PSVersionTable.PSVersion

1. Verify execution of scripts is allowed with `Get-ExecutionPolicy` (should be `RemoteSigned` or `Unrestricted`). If scripts are not enabled, run PowerShell as Administrator and call `Set-ExecutionPolicy RemoteSigned -Scope CurrentUser -Confirm`.

2. Verify that `git` can be run from PowerShell. If the command is not found, you will need to add a git alias or add `%ProgramFiles%\Git\cmd` to your PATH environment variable.

3. Clone the PoshGet repository to your `~\documents\WindowsPowerShell\modules` directory or any directory in your `$env:PsModulePath`.

4. Run `Import-Module PoshGet` and/or at that to your PowerShell profile.

5. Enjoy!

Warning
-------

PoshGet is currently under initial development. Use at your own risk. This notice will be removed when the module is considered ready for wide spread usage.